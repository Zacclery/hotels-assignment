# Hotel Assignment
# Version: 1.0
FROM python:3

RUN apt-get update && apt-get upgrade -y && apt-get autoremove && apt-get autoclean && apt-get -y install cron

# Project Files and Settings
ARG PROJECT=hotel_assignment
ARG PROJECT_DIR=/var/www/${PROJECT}
RUN mkdir -p $PROJECT_DIR
WORKDIR $PROJECT_DIR
COPY requirements.txt .
RUN pip install -r requirements.txt

# Server
EXPOSE 8000
STOPSIGNAL SIGINT
ENTRYPOINT ["python", "manage.py"]
CMD ["runserver", "0.0.0.0:8000"]
