from django.core.management.base import BaseCommand

from app.utils import get_csv, save_city_data, save_hotel_data
from hotels_assignment.settings import CITY_URL, HOTEL_URL


class Command(BaseCommand):
    help = 'Retrieves and inserts CSV into DB'

    def handle(self, *args, **options):
        city = get_csv(CITY_URL)
        save_city_data(city)
        hotel = get_csv(HOTEL_URL)
        save_hotel_data(hotel)
