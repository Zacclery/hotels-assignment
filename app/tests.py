from django.test import Client
from django.test import TestCase
from app.utils import get_csv, save_hotel_data, save_city_data, get_next_free_hotel_id
from hotels_assignment.settings import CSV_PWD, CSV_USER, CITY_URL, HOTEL_URL
import pandas as pd


class HomePageTest(TestCase):

    def setUp(self):
        self.client = Client()

    def test_get_index_resolves(self):
        response = self.client.get('/hotels/index/')
        self.assertEqual(response.status_code, 200)

    def test_index_content(self):
        response = self.client.get('/hotels/index/')
        html = response.content.decode('utf8')
        self.assertTrue(html.startswith('<!DOCTYPE html>'))
        self.assertIn('<title>Cities and Hotels</title>', html)


class UtilsTest(TestCase):

    def setUp(self):
        self.city_df = get_csv(CITY_URL)
        self.hotel_df = get_csv(CITY_URL)

    def test_csv_returned(self):
        self.assertIsInstance(self.city_df, pd.DataFrame)
        self.assertIsInstance(self.hotel_df, pd.DataFrame)

        self.assertFalse(self.city_df.empty)
        self.assertFalse(self.hotel_df.empty)
