import re
from io import StringIO

import pandas as pd
import requests
from requests.auth import HTTPBasicAuth

from app.models import Hotel, City
from hotels_assignment.settings import CSV_PWD, CSV_USER


def get_csv(loc):
    try:
        # try access using url with authentication, else use local version
        response = requests.get(loc, auth=HTTPBasicAuth(CSV_USER, CSV_PWD))
        assert response.status_code == 200
        df = pd.read_csv(StringIO(response.text), encoding="utf-8-sig", sep=';', dtype=str, header=None)
        file = loc.rsplit('/', 1)[1]
        df.to_csv('data/' + file, sep=';', encoding='utf-8-sig')
        return df
    except (requests.exceptions.MissingSchema, requests.exceptions.ConnectionError, AssertionError):
        try:
            file = loc.rsplit('/', 1)[1]
            return pd.read_csv('data/' + file, encoding="utf-8-sig", sep=';', dtype=str, header=None)
        except FileNotFoundError:
            # try download manually
            return None


def save_hotel_data(df):
    for i in range(len(df)):
        try:
            city_code, hotel_code, hotel_name = df.iloc[i][0], df.iloc[i][1], df.iloc[i][2]
            city = City.objects.get_or_create(code=city_code)[0]
            Hotel.objects.get_or_create(city=city, code=hotel_code, name=hotel_name)

        except ValueError:
            # invalid data row
            continue


def save_city_data(df):
    for i in range(len(df)):
        try:
            city_code, city_name = df.iloc[i][0], df.iloc[i][1]
            City.objects.update_or_create(code=city_code, name=city_name)
        except ValueError:
            # invalid data row
            continue


def get_next_free_hotel_id(city):
    # could be made more intelligent by lowest available id instead
    hotels = Hotel.objects.filter(city__name__iexact=city).values()
    existing_nums = []
    for hotel in hotels:
        match = re.match(r"([a-z]+)([0-9]+)", hotel['code'], re.I)
        if match:
            city_code, num = match.groups()
            existing_nums.append(num)

    if existing_nums is not None:
        new_num = str(int(max(existing_nums)) + 1)
        new_code = city_code + '0' + new_num if len(new_num) == 1 else city_code + new_num
        return new_code
    return None
