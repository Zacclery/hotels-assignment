# Hotels Project
A simple application for viewing, retrieving and editing hotel data, per city.

## Getting Started
```
git clone https://zacec@bitbucket.org/zacec/hotels_assignment.git
cd hotels_assignment
docker-compose build
docker-compose up
```
http://127.0.0.1:8000/hotels/index/

### Prerequisites
In order to run from container, requires docker and docker-compose installation.
Data can be retrieved remotely by running
```
python manage.py import_data
```
However there seemed to be some inconsistencies regarding success of requests to the server.

## Running the tests
```
python manage.py test
```

## Author
Zac Clery 
